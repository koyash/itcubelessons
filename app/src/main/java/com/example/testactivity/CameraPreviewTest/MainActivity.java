package com.example.testactivity.CameraPreviewTest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageDecoder;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.testactivity.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private int PICK_PHOTO_CODE = 10;
    private Camera camera;
    private ImageButton capturePhoto;
    private Button backBtn, finishBtn, filesBtn;
    private SurfaceView surface;
    private CameraSurface cameraSurface;
    private LinearLayout linearLayout;
    private ArrayList<Uri> arrayList;
    private boolean isCapturing = false;
    private HorizontalScrollView hz;
    private int height;
    private int width;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_camera);

        arrayList = new ArrayList<>();

        hz = findViewById(R.id.hz);


        backBtn = findViewById(R.id.backBtn);
        finishBtn = findViewById(R.id.readyBtn);
        filesBtn = findViewById(R.id.filesBtn);

        linearLayout = findViewById(R.id.linearScroller);

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //если разрешение на считывание нет, то спрашиваем
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1);

        }
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //если разрешение на считывание нет, то спрашиваем
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    2);
        }
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //если разрешение на считывание нет, то спрашиваем
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    3);
        }
        surface = findViewById(R.id.surfaceView);

        camera = Camera.open();

        cameraSurface = new CameraSurface(MainActivity.this, camera, surface.getHolder());

        capturePhoto = findViewById(R.id.captureBtn);
        capturePhoto.setOnClickListener(v -> {
            if (!isCapturing) {
                isCapturing = true;
                cameraSurface.mcamera.lock();
                cameraSurface.mcamera.takePicture(null, null, asd);
            }
        });

        finishBtn.setOnClickListener(v->{
            Bundle b = new Bundle();
            b.putSerializable("images", arrayList);
            Intent i = new Intent();
            i.putExtras(b);
            setResult(RESULT_OK, i);
            finish();
        });

        backBtn.setOnClickListener(v->{
            finish();
        });

        filesBtn.setOnClickListener(v->{
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            if (intent.resolveActivity(getPackageManager()) != null) {
                // Bring up gallery to select a photo
                startActivityForResult(intent, PICK_PHOTO_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_CODE){
            Uri uri = data.getData();
            Bitmap image = null;
            if(Build.VERSION.SDK_INT > 27){
                // on newer versions of Android, use the new decodeBitmap method
                ImageDecoder.Source source = ImageDecoder.createSource(this.getContentResolver(), uri);
                try {
                    image = ImageDecoder.decodeBitmap(source);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                // support older versions of Android by using getBitmap
                try {
                    image = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            height = hz.getHeight();
            width = hz.getHeight();
            image=Bitmap.createScaledBitmap(image, width,height, true);
            AddPhoto(image, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    Camera.PictureCallback asd = new Camera.PictureCallback(){
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            cameraSurface.mcamera.startPreview();
            try {
                height = hz.getHeight();
                width = hz.getHeight();
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                bitmap=Bitmap.createScaledBitmap(bitmap, width,height, true);

                AddPhoto(bitmap, 90);

                Log.d("MYTAG", "onPictureTaken - wrote bytes: " + data.length);
                isCapturing = false;
            }  catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
            Log.d("MYTAG", "onPictureTaken - jpeg");
        }
    };

    public void AddPhoto(Bitmap bitmap, int rotation){
        ImageView img = new ImageView(MainActivity.this);
        img.setImageBitmap(bitmap);
        img.setMaxHeight(100);
        img.setMaxWidth(100);
        img.setRotation(rotation);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(30, 0, 0, 0);
        linearLayout.addView(img, layoutParams);

        arrayList.add(getImageUri(MainActivity.this, bitmap));

        hz.fullScroll(View.FOCUS_DOWN);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

}