package com.example.testactivity.CameraPreviewTest;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.testactivity.R;

import java.io.Serializable;
import java.util.ArrayList;

public class StartActivity extends AppCompatActivity {

    Button btn;
    ImagesAdapter adapter;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        btn = findViewById(R.id.startBtn1);

        btn.setOnClickListener(v->{
            Intent i = new Intent(StartActivity.this, MainActivity.class);
            startActivityForResult(i, 1);
        });

        gridView = findViewById(R.id.gridView);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null){
            Bundle o = data.getExtras();
            ArrayList<Uri> paths = (ArrayList<Uri>)o.getSerializable("images");
            gridView.setAdapter(new ImagesAdapter(getApplicationContext(), paths));
        }
    }

}