package com.example.testactivity.CameraPreviewTest;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {
    Camera mcamera;
    SurfaceHolder holder;

    public CameraSurface(Context context, Camera camera, SurfaceHolder holder) {
        super(context);
        this.mcamera = camera;
        this.holder = holder;
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        try {
            startCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startCamera() throws Exception{
        Camera.Parameters params = mcamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mcamera.setParameters(params);
        mcamera.setDisplayOrientation(90);
        mcamera.setPreviewDisplay(this.holder);
        mcamera.startPreview();
    }


    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }

}