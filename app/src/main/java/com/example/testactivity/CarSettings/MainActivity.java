 package com.example.testactivity.CarSettings;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.testactivity.R;

public class MainActivity extends AppCompatActivity {

    ImageView leftLight, carImage, rightLight, doorImage;

    public class Car{
        public Door door = new Door();
        public Light leftLight = new Light(), rightLight = new Light();
        public Car(){
            System.out.println("Car created!");
        }
        // дверь
        class Door{
            boolean state = false;
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public void open(){
                state = true;
            }//открыть
            public void close(){
                state = false;

            }// закрыть
        }  // свет
        class Light {
            boolean state = false;
            public void on(){
                state = true;
            }// приоткрыть
            public void off(){
                state = false;
            }// опустить
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        leftLight = findViewById(R.id.leftLightImage);
        rightLight = findViewById(R.id.rightLightImage);
        carImage = findViewById(R.id.carImage);
        doorImage = findViewById(R.id.doorImage);
        Car c = new Car();
        if (!c.leftLight.state){
            leftLight.setImageDrawable(getDrawable(R.drawable.off));
        }
        if (!c.rightLight.state){
            rightLight.setImageDrawable(getDrawable(R.drawable.off));
        }
        if (!c.door.state){
            doorImage.setImageDrawable(getDrawable(R.drawable.off));
        }
        leftLight.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View v) {
                if (!c.leftLight.state){
                    c.leftLight.on();
                    leftLight.setImageDrawable(getDrawable(R.drawable.leftlight));
                } else {
                    c.leftLight.off();
                    leftLight.setImageDrawable(getDrawable(R.drawable.off));
                }
            }
        });

        rightLight.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (!c.rightLight.state){
                    c.rightLight.on();
                    rightLight.setImageDrawable(getDrawable(R.drawable.rightlight));
                } else {
                    c.rightLight.off();
                    rightLight.setImageDrawable(getDrawable(R.drawable.off));
                }
            }
        });

        doorImage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if (!c.door.state){
                    c.door.open();
                    doorImage.setImageDrawable(getDrawable(R.drawable.door));
                } else {
                    c.door.close();
                    doorImage.setImageDrawable(getDrawable(R.drawable.off));
                }
            }
        });

        carImage.setOnClickListener(v -> {
            //это наш диалог
            Dialog d = null;
            //это наш конструктор
            AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
            //здесь берем окружение которое будем отрисовывать. my_dialog это мой xml файл, вам нужен свой
            View dialogView = LayoutInflater.from(MainActivity.this).inflate(R.layout.my_dialog, null);
            //ставим наше окружение
            b.setView(dialogView);


            Button b1, b2, b3;
            b1 = dialogView.findViewById(R.id.dialogbtn1);
            b2 = dialogView.findViewById(R.id.dialogbtn2);
            b3 = dialogView.findViewById(R.id.dialogbtn3);

            b1.setText(c.leftLight.state == false ? "Включить левую":"Выключить левую");

            b1.setOnClickListener(v1 -> {
                if (!(c.leftLight.state)) {
                    c.leftLight.on();
                    leftLight.setImageDrawable(getDrawable(R.drawable.leftlight));
                } else {
                    c.leftLight.off();
                    rightLight.setImageDrawable(getDrawable(R.drawable.off));
                }
            });
            //рисуем диалог
            d = b.create();
            d.show();

/*
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Title");
            String aboutDoor, aboutLeft, aboutRight;
            if (c.door.state)
                aboutDoor = "Закрыть дверь";
            else
                aboutDoor = "Открыть дверь";
            if (c.leftLight.state){
                aboutLeft = "Выключить левую фару";
            } else {
                aboutLeft = "Включить левую фару";
            } if (c.rightLight.state){
                aboutRight = "Выключить правую фару";
            } else {
                aboutRight = "Включить правую фару";
            }
            builder.setItems(new CharSequence[]
                            {aboutDoor, aboutLeft, aboutRight},
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    if (!c.door.state){
                                        c.door.open();
                                        doorImage.setImageDrawable(getDrawable(R.drawable.door));
                                    } else {
                                        c.door.close();
                                        doorImage.setImageDrawable(getDrawable(R.drawable.off));
                                    }
                                    break;
                                case 1:
                                    if (!c.leftLight.state){
                                        c.leftLight.on();
                                        leftLight.setImageDrawable(getDrawable(R.drawable.leftlight));
                                    } else {
                                        c.leftLight.off();
                                        leftLight.setImageDrawable(getDrawable(R.drawable.off));
                                    }
                                    break;
                                case 2:
                                    if (!c.rightLight.state){
                                        c.rightLight.on();
                                        rightLight.setImageDrawable(getDrawable(R.drawable.rightlight));
                                    } else {
                                        c.rightLight.off();
                                        rightLight.setImageDrawable(getDrawable(R.drawable.off));
                                    } break;
                            }
                        }
                    });
            builder.create().show();*/
        });

    }
}