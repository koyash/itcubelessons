package com.example.testactivity.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testactivity.R;

import java.util.ArrayList;

public class MyCustomAdapter extends RecyclerView.Adapter<MyCustomAdapter.MyAdapter> {

    public class MyAdapter extends RecyclerView.ViewHolder{

        TextView t1, t2;
        Button btn;
        CardView cv;

        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            t1 = itemView.findViewById(R.id.textInCard);
            t2 = itemView.findViewById(R.id.randTextCard);
            btn = itemView.findViewById(R.id.btnCard);
            cv = itemView.findViewById(R.id.cv);
        }
    }

    ArrayList<MyClassForTestObjects> arr;

    public MyCustomAdapter(ArrayList<MyClassForTestObjects> arr){
        this.arr = arr;
    }

    @NonNull
    @Override
    public MyAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_card, parent, false);
        MyCustomAdapter.MyAdapter myAdapter = new MyAdapter(v);
        return myAdapter;
    }

    @Override
        public void onBindViewHolder(@NonNull MyAdapter holder, int position) {
        holder.t1.setText(arr.get(position).str1);
        holder.t2.setText(arr.get(position).str2);
        holder.btn.setOnClickListener(v -> {
            holder.t1.setText("");
        });
        holder.cv.setOnClickListener(v -> {
            Toast.makeText(v.getContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }
}
