package com.example.testactivity.Adapters;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testactivity.R;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ArrayList<MyClassForTestObjects> arr = new ArrayList<>();
    RecyclerView rv;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adapter_example);
        for (int i = 0; i < 10; i++){
            arr.add(new MyClassForTestObjects(i,"я строка №" + i, String.valueOf(new Random().nextInt())));
        }

        rv = findViewById(R.id.myRV);

        MyCustomAdapter adapter = new MyCustomAdapter(arr);

        layoutManager = new LinearLayoutManager(MainActivity.this);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);
    }

}
