package com.example.testactivity.MyDB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyOpenHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;

    public MyOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        String qUser = "CREATE TABLE user" +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "email TEXT," +
                "pass TEXT)";
        db.execSQL(qUser);

        String qBook = "CREATE TABLE book" +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT," +
                "price TEXT," +
                "categoryId INTEGER," +
                "FOREIGN KEY(categoryId) REFERENCES category(id))";
        db.execSQL(qBook);

        String qCategory = "CREATE TABLE category" +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT)";
        db.execSQL(qCategory);

        db.execSQL("INSERT INTO user(email, pass) VALUES ('roma','leha')");

        db.execSQL("INSERT INTO category(name) VALUES('horror')");
        db.execSQL("INSERT INTO category(name) VALUES('science fiction')");

        db.execSQL("INSERT INTO book(name, price, categoryId) VALUES('IT',500,1)");
        db.execSQL("INSERT INTO book(name, price, categoryId) VALUES('Karry',1500,1)");
        db.execSQL("INSERT INTO book(name, price, categoryId) VALUES('STALKER',800,2)");
        db.execSQL("INSERT INTO book(name, price, categoryId) VALUES('METRO 2033',1200,2)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }




}
