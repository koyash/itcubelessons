package com.example.testactivity.MyDB;

public class Book {
    int price;
    String name;
    String category;
    public Book(String name, String category, int price){
        this.name = name;
        this.price = price;
        this.category = category;
    }
}
