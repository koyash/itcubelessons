package com.example.testactivity.MyDB;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.testactivity.Adapters.MainActivity;
import com.example.testactivity.Adapters.MyCustomAdapter;
import com.example.testactivity.R;

import java.util.ArrayList;

public class Books extends AppCompatActivity {

    RecyclerView rv;
    SQLiteDatabase db;
    ArrayList<Book> arr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        db = DBActivity.dbHelper.getReadableDatabase();

        arr = new ArrayList<>();

        Cursor bookCursor = db.rawQuery("SELECT * FROM book", new String[]{});
        int n = bookCursor.getCount();
        int i = 0;
        while (i < n){
            bookCursor.moveToPosition(i);

            Cursor categoryCursor = db.rawQuery("SELECT name FROM category WHERE id = ?",
                    new String[]{bookCursor.getString(3)});
            categoryCursor.moveToPosition(0);
            String category = categoryCursor.getString(0);

            Book b = new Book(bookCursor.getString(1), category, bookCursor.getInt(2));
            arr.add(b);
            i++;
        }

        rv = findViewById(R.id.booksRV);

        BooksAdapter adapter = new BooksAdapter(arr);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Books.this);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);

    }
}