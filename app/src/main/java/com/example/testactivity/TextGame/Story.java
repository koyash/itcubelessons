package com.example.testactivity.TextGame;

public class Story {
    private Situation start_story;
    public Situation current_situation;

    Story() {
        String[] variants = new String[]{"У клиента денег много, а у меня нет - вы выпишете ему счет на 120 коробок ULTIMATE по 50тр","Чуть дороже сделаем, кто там заметит - вы выпишете ему счет на 100 коробок PRO по 10тр","Как надо так и сделаем - вы выпишете ему счет на 100 коробок HOME по 5тр "};
        String[] variants1 = new String[]{"Познакомиться с Гошей", "Познакомиться с Дашей", "Ни с кем не знакомиться"};
        String[] variants2 = new String[]{"Пойти праздновать!", "Пойти отдохнуть!", "Работать ещё больше!"};
        String[] variants3 = new String[]{"Поблагодарить босса за предоставленную возможность!", "Отказаться!", "Пойти подготовиться"};
        start_story = new Situation(
                "Только вы начали работать и тут же удача! Вы нашли клиента и продаете ему "
                        + "партию ПО MS Windows. Ему в принципе достаточно взять 100 коробок версии HOME.\n",
                3, 0, 0, 0, variants);

        //первая ветка

        start_story.direction[0].direction[0] = new Situation(
                "Это друг босса! Вы подружились и получили продвежение по карьере!",
                0, 1, 2, 0, null);
        start_story.direction[0].direction[1] = new Situation(
                "Даша - уборщица. Вы просто завели нового друга!",
                0, 0, 0, 0, null);
        start_story.direction[0].direction[2] = new Situation(
                "Вы впали в депрессию от одиночества и проиграли!",
                0, -1000, -1000, -1000, null);

        //вторая ветка
        start_story.direction[1] = new Situation(
                "Сегодня будет совещание, меня неожиданно вызвали,"
                        + "есть сведения что босс доволен сегодняшней сделкой.",
                3, 1, 100, 0, variants2);
        start_story.direction[1].direction[0] = new Situation(
                "После празднования вы устроили дебош! Вас уволили...",
                0, -1000, -1000, -1000, null);
        start_story.direction[1].direction[1] = new Situation(
                "После хорошего отдыха вы наладили больше контактов!",
                0, 1, 0, 5, null);
        start_story.direction[1].direction[2] = new Situation(
                "Вы впали в депрессию от переутомления и проиграли!",
                0, -1000, -1000, -1000, null);


        //третья ветка
        start_story.direction[2] = new Situation(
                "Мой первый клиент доволен скоростью и качеством "
                        + "моей работы. Сейчас мне звонил лично директор компании,  сообщил что скоро состоится еще более крупная сделка"
                        + " и он хотел, чтобы по ней работал именно я!",
                3, 0,50, 1, variants3);
        start_story.direction[2].direction[0] = new Situation(
                "Босс принял вашу благодарность и повышает вас, как старательного работника!",
                0, 1, 0, 0, null);
        start_story.direction[2].direction[1] = new Situation(
                "Босс не доволен!",
                0, -1, -30, 0, null);
        start_story.direction[2].direction[2] = new Situation(
                "После подготовки встреча прошла очень успешно! Босс доволен!",
                0, 10, 100, 5, null);

        current_situation = start_story;
    }

    public void go(int num) {
        if (num <= current_situation.direction.length)
            current_situation = current_situation.direction[num - 1];
        else
            System.out.println("Вы можете выбирать из "
                    + current_situation.direction.length + " вариантов");
    }

    public boolean isEnd() {
        return current_situation.direction.length == 0;
    }
}
