package com.example.testactivity.testMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.testactivity.R;
import com.example.testactivity.testBottomSheet.BottomSheetDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, BottomSheetDialog.SheetListener {

    GoogleMap map;

    Button findBtn;
    EditText nEdit, eEdit;

    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        smf.getMapAsync(this);
        nEdit = findViewById(R.id.Ntext);
        eEdit = findViewById(R.id.Etext);

        findBtn = findViewById(R.id.findBtn);

        dialog = createDialog();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public AlertDialog createDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(getApplicationContext());
        b.setMessage("Пожалуйста, подождите идёт загрузка!");
        b.setTitle("Загрузка!");
        b.setCancelable(false);
        return b.create();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        findBtn.setOnClickListener(v -> {
            if (!nEdit.getText().toString().equals("") && !eEdit.getText().toString().equals("")) {
                LatLng latLng = new LatLng(Double.parseDouble(nEdit.getText().toString()),Double.parseDouble(eEdit.getText().toString()));
                map.addMarker(new MarkerOptions().position(latLng).title("Searching place!"));
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });
        map.setOnMapClickListener(this);
//
//        LatLng azn = new LatLng(54.871905, 53.038985);
//        map.addMarker(new MarkerOptions().position(azn).title("Aznakaevo"));
//        map.moveCamera(CameraUpdateFactory.newLatLng(azn));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        BottomSheetDialog bsd = new BottomSheetDialog(latLng);
        map.clear();
        map.addMarker(new MarkerOptions().position(latLng));
        bsd.show(getSupportFragmentManager(), "showMapCoor");
    }

    @Override
    public void onClick(String text) {

    }
}