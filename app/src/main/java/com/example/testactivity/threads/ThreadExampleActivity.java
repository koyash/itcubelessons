 package com.example.testactivity.threads;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.testactivity.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ThreadExampleActivity extends AppCompatActivity {
    Button bStart, btJustDoIt;
    ProgressBar progressBar;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_example);
        bStart = (Button) findViewById(R.id.btStart);
        btJustDoIt= (Button) findViewById(R.id.btJustDoIt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        text = (TextView) findViewById(R.id.text);
        // устанавливаем обработчик на кнопку "Начать в потоке"
        bStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ThreadExampleActivity.this, "Делаем операцию в потоке (10с)", Toast.LENGTH_SHORT).show();

                new MyLoadImageAsync().execute("https://jrnlst.ru/sites/default/files/covers/cover_6.jpg"); //вот так запустить из главного потока
            }
        });
        // устанавливаем обработчик на кнопку "Начать не в потоке"
        btJustDoIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ThreadExampleActivity.this, "Делаем операцию не в потоке (10с)", Toast.LENGTH_SHORT).show();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    

    private class MyLoadImageAsync extends AsyncTask<String, Void, Bitmap>{
        Dialog d = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AlertDialog.Builder b = new AlertDialog.Builder(ThreadExampleActivity.this);
            b.setTitle("Загрузка!");
            b.setMessage("Падажи идет загрузка!");
            d = b.create();
            d.show();
        }

        @Override
        protected Bitmap doInBackground(String... voids) {
            URL u = null;
            try {
                u = new URL(voids[0]);
                InputStream is = (InputStream) u.getContent(); //грузим из интернета
                Bitmap b = BitmapFactory.decodeStream(is); //преврщаем в bitmap
                return b;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) { //получаем bitmap загруженной картинки
            super.onPostExecute(bitmap);
            d.cancel();
            if (bitmap != null){

                ImageView img = findViewById(R.id.imageView3);
                img.setImageBitmap(bitmap);
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }
    }

}