package com.example.testactivity.testBottomSheet;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.testactivity.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetDialog  extends BottomSheetDialogFragment {
    TextView tv;
    Button btn;

    private SheetListener myListener;

    LatLng latLng;

    public BottomSheetDialog( LatLng latLng){
        this.latLng = latLng;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet, container, false);

        tv = v.findViewById(R.id.bottomSheetText);

        tv.setText(latLng.latitude + " " + latLng.longitude);

        btn = v.findViewById(R.id.tapperBtn);
        btn.setOnClickListener(btnView ->{
            dismiss();
        });

        return v;

    }
    public interface SheetListener{
        void onClick(String text);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        myListener = (SheetListener) context;
    }
}
