package com.example.testactivity.Arthur;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.example.testactivity.R;

public class TestActivity extends AppCompatActivity {

    SQLiteDatabase db;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        DbHelper dbHelper = new DbHelper(this, "testDbArthur", null, 1);
        db = dbHelper.getReadableDatabase();
        String[] arraySpinner = new String[] {
                "Pushkin", "Lermontov", "Gluhovsky", "Tolstoy", "Gogol", "Esenin", "Rihter"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        spinner = findViewById(R.id.spinner123);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) spinner.getItemAtPosition(position);

                ContentValues values = new ContentValues();
                values.put("idSubject", selected);

                db.insert("author",null, values);

                Cursor c = db.rawQuery("select * from author", null);
                while(c.moveToNext()){
                    Toast.makeText(TestActivity.this, c.getString(1), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Cursor c = db.rawQuery("select * from author", null);
                while(c.moveToNext()){
                    Toast.makeText(TestActivity.this, c.getString(1), Toast.LENGTH_SHORT).show();
                }
            }
        });
        }
}

class DbHelper extends SQLiteOpenHelper{

    public DbHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE author(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
