package com.example.testactivity.testIntent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testactivity.R;

public class RegistrationActivity extends AppCompatActivity {

    Button signUp;
    EditText log, pass;
    EditText name, surname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        log = findViewById(R.id.loginEditText);
        pass = findViewById(R.id.passEditText);
        name = findViewById(R.id.nameEditText);
        surname = findViewById(R.id.surnameEditText);
        signUp = findViewById(R.id.button);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent res = new Intent();
                res.putExtra("login", log.getText().toString());
                res.putExtra("pass", pass.getText().toString());
                res.putExtra("name", name.getText().toString());
                res.putExtra("surname", surname.getText().toString());
                setResult(RESULT_OK, res);
                finish();
            }
        });
    }
}