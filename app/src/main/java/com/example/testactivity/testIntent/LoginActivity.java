package com.example.testactivity.testIntent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testactivity.R;

public class LoginActivity extends AppCompatActivity {

    private final int REGISTRCODE = 1, SOMACTIVITY = 12;

    User[] uArr;
    int count = 0, maxCount = 3;
    Button signUpBtn, signInBtn;

    EditText pass, log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        log = findViewById(R.id.logEdit);
        pass = findViewById(R.id.passEdit);
        uArr = new User[maxCount];
        signUpBtn = findViewById(R.id.signUpbtn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent registrIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivityForResult(registrIntent, REGISTRCODE);
            }
        });
        signInBtn = findViewById(R.id.signInBtn);
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean found = false;
                for (int i = 0; i < count; i++) {
                    if (uArr[i].login.equals(log.getText().toString()) && uArr[i].pass.equals(pass.getText().toString())){
                        found = true;
                        Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                        String msg = "id = " + i + " name = " + uArr[i].name + " surname = " + uArr[i].surname;
                        intent.putExtra("info", msg);
                        startActivity(intent);
                        break;
                    }
                }
                if (!found) {
                    Toast.makeText(LoginActivity.this, "Такого пользователя нет!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REGISTRCODE) {
            if (resultCode == RESULT_OK) {
                if (count < maxCount) {
                    uArr[count] = new User(data.getStringExtra("login"), data.getStringExtra("pass"),
                            data.getStringExtra("name"), data.getStringExtra("surname"));
                    count++;
                }
                else {
                    Toast.makeText(LoginActivity.this, "Свободных мест нет!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class User {
        String login, pass, name, surname;

        User(String login, String pass, String name, String surname) {
            this.login = login;
            this.pass = pass;
            this.name = name;
            this.surname = surname;
        }
    }

}