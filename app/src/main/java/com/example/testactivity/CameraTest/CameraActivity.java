package com.example.testactivity.CameraTest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.textclassifier.ConversationActions;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.testactivity.R;

import java.security.Permission;
import java.security.Permissions;

public class CameraActivity extends AppCompatActivity {

    public int IMAGECAPTUREKEY = 101;
    Button captureBtn;
    LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        linearLayout = findViewById(R.id.scrollerLiner);

        captureBtn = findViewById(R.id.captureImgBtn);

        captureBtn.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(CameraActivity.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                //если разрешение на считывание нет, то спрашиваем
                ActivityCompat.requestPermissions(CameraActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        IMAGECAPTUREKEY);

            } else {
                //иначе идем смотреть фотки
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), IMAGECAPTUREKEY);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == IMAGECAPTUREKEY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), IMAGECAPTUREKEY);
            } else {
                Toast.makeText(CameraActivity.this, "ДАЙТЕ РАЗРЕШЕНИЕ ВЫ ЧО", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGECAPTUREKEY) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView img = new ImageView(CameraActivity.this);
            img.setImageBitmap(imageBitmap);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(30, 0, 0, 0);
            linearLayout.addView(img, layoutParams);
        }
    }
}