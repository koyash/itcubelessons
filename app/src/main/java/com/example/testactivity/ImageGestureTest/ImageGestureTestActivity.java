package com.example.testactivity.ImageGestureTest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.testactivity.R;

public class ImageGestureTestActivity extends AppCompatActivity {

    ConstraintLayout layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gesture_test);

        layout = findViewById(R.id.myRelate);

        MyCustomImage image = new MyCustomImage(this, R.drawable.smile, layout);
        MyCustomImage image1 = new MyCustomImage(this, R.drawable.maincar, layout);
        image1.setX(400);
        image1.setY(400);
    }



}