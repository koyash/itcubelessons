package com.example.testactivity.RecyclerViewTest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testactivity.Adapters.MyClassForTestObjects;
import com.example.testactivity.Adapters.MyCustomAdapter;
import com.example.testactivity.R;

import java.util.ArrayList;

public class RecyclerHelper extends RecyclerView.Adapter<RecyclerHelper.MyAdapter> {

    public class MyAdapter extends RecyclerView.ViewHolder {
        TextView intTv, stringTv, booleanTv;
        public MyAdapter(@NonNull View itemView) {
            super(itemView);
            intTv = itemView.findViewById(R.id.intTV);
            stringTv = itemView.findViewById(R.id.stringTV);
            booleanTv = itemView.findViewById(R.id.booleanTV);
        }
    }

    ArrayList<MyData> arr;

    public RecyclerHelper(ArrayList<MyData> arr) {
        this.arr = arr;
    }

    @NonNull
    @Override
    public RecyclerHelper.MyAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_card_recycler, parent, false);
        MyAdapter myAdapter = new MyAdapter(v);
        return myAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter holder, int position) {
        holder.stringTv.setText(arr.get(position).someString);
        holder.intTv.setText(String.valueOf(arr.get(position).someInt));
        holder.booleanTv.setText(String.valueOf(arr.get(position).someBoolean));
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }
}