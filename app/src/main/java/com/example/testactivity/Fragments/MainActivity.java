package com.example.testactivity.Fragments;

import android.mtp.MtpConstants;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.testactivity.R;

public class MainActivity extends AppCompatActivity {
    Button btnOpener;
    FragmentManager fm = getSupportFragmentManager();
    FragmentTransaction ft;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_for_fragment);
        btnOpener = findViewById(R.id.buttonToOpenFragment);
        btnOpener.setOnClickListener(v -> {
            ft = fm.beginTransaction();
            MyFragment myfr = new MyFragment();
            ft.replace(R.id.place_holder, myfr);
            ft.commit();
        });
    }

}
