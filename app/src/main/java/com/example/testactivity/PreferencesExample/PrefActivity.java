package com.example.testactivity.PreferencesExample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testactivity.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PrefActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText nameEdit;
    Button saveBtn, loadBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);
        sharedPreferences = getSharedPreferences("myPref", MODE_PRIVATE);
        saveBtn = findViewById(R.id.saveBtn);
        loadBtn = findViewById(R.id.loadBtn);
        nameEdit = findViewById(R.id.forgetNameEdit);
        Map<String, Integer> set = new HashMap<>();
        set.put("id", 1);
        set.put("id1", 2);
        SharedPreferences.Editor editor1 = sharedPreferences.edit();
        editor1.putStringSet("ids", set.keySet());
        editor1.apply();
        saveBtn.setOnClickListener(v -> {
            if (!nameEdit.getText().toString().equals("")){
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("name", nameEdit.getText().toString());
                editor.apply();
                Toast.makeText(this,"Saved", Toast.LENGTH_SHORT).show();
            }
        });
        loadBtn.setOnClickListener(v -> {
            nameEdit.setText(sharedPreferences.getString("name", "no name"));
            Toast.makeText(this,"Showed", Toast.LENGTH_SHORT).show();
        });
    }
}