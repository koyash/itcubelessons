package com.example.testactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class resgisrtActivity extends AppCompatActivity {

    int count = 0, maxCount = 5;
    User[] userArr;

    Button addBtn, showBtn;
    EditText name;
    CheckBox carCheck;
    CalendarView cv;


    final String[] finalDate = {""};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resgisrt);

        //указываем количество пользователей
        userArr = new User[maxCount];

        //указавыем кнопки и прочее
        addBtn = findViewById(R.id.addBtn);
        name = findViewById(R.id.nameEdit);
        carCheck = findViewById(R.id.carCheck);
        showBtn = findViewById(R.id.showBtn);
        cv = findViewById(R.id.meetingCalendar);

        //код
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("")){
                    Toast.makeText(resgisrtActivity.this, "Не заполнены поля!", Toast.LENGTH_LONG).show();
                } else {
                    if (count < maxCount) {
                        userArr[count] = new User(name.getText().toString(), carCheck.isChecked(), finalDate[0]);
                        count++;
                        name.setText("");
                        carCheck.setChecked(false);
                    } else {
                        Toast.makeText(resgisrtActivity.this, "Нет свободных мест!", Toast.LENGTH_LONG).show();

                    }
                }
            }
        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(resgisrtActivity.this);
                builder.setTitle("Информация!");
                String msg = "";
                for (int i = 0; i < count; i++){
                    msg += userArr[i].name + ". He has a car = " + userArr[i].hasCar + ". Prefer date = " + userArr[i].dateMeeting + "\n\n";
                }
                builder.setMessage(msg);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                int mYear = year;
                int mMonth = month;
                int mDay = dayOfMonth;
                String selectedDate = new StringBuilder().append(mYear)
                        .append("-").append(mMonth + 1).append("-").append(mDay)
                        .toString();
                finalDate[0] = selectedDate;
            }
        });

    }

    public class User{
        String name;
        boolean hasCar;
        String dateMeeting;
        User(String name, boolean hasCar, String dateMeeting){
            this.name = name;
            this.hasCar = hasCar;
            this.dateMeeting = dateMeeting;
        }
    }

}